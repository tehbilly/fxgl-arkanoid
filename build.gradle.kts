plugins {
    java
    application
    kotlin("jvm") version "1.3.71"
    id("org.openjfx.javafxplugin") version "0.0.8"
    id("org.beryx.runtime") version "1.8.1"
}

group = "net.tehbilly"
version = "1.0.0"

repositories {
    mavenCentral()
    jcenter()
    maven(url = "https://nexus.gluonhq.com/nexus/content/repositories/releases")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation("com.github.almasb:fxgl:11.8")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}

javafx {
    version = "11.0.1"
    modules = listOf("javafx.controls", "javafx.fxml", "javafx.swing", "javafx.media")
}

application {
    mainClassName = "net.tehbilly.arkanoid.BasicGame"
}

runtime {
    options.addAll("--strip-debug", "--compress=2", "--no-header-files", "--no-man-pages")
}