package net.tehbilly.arkanoid

import com.almasb.fxgl.dsl.entityBuilder
import com.almasb.fxgl.entity.Entity
import com.almasb.fxgl.entity.EntityFactory
import com.almasb.fxgl.entity.SpawnData
import com.almasb.fxgl.entity.Spawns
import javafx.geometry.Point2D
import javafx.scene.paint.Color
import javafx.scene.shape.Rectangle

class ArkanoidEntityFactory : EntityFactory {
    @Spawns("ball")
    fun newBall(data: SpawnData): Entity = entityBuilder()
        .from(data)
        .type(EntityTypes.BALL)
        .viewWithBBox(Rectangle(BALL_SIZE, BALL_SIZE, Color.BLUE))
        .collidable()
        .with("velocity", Point2D(BALL_SPEED, -BALL_SPEED))
        .build()

    @Spawns("brick")
    fun newBrick(data: SpawnData): Entity = entityBuilder()
        .from(data)
        .type(EntityTypes.BRICK)
        .viewWithBBox(Rectangle(BRICK_WIDTH, BRICK_HEIGHT, Color.LIGHTSTEELBLUE))
        .collidable()
        .build()
}