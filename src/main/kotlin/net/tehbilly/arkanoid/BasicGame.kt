package net.tehbilly.arkanoid

import com.almasb.fxgl.app.GameApplication
import com.almasb.fxgl.app.GameSettings
import com.almasb.fxgl.core.math.FXGLMath
import com.almasb.fxgl.dsl.*
import com.almasb.fxgl.entity.Entity
import com.almasb.fxgl.input.Input
import com.almasb.fxgl.input.UserAction
import com.almasb.fxgl.ui.FontType
import javafx.geometry.Point2D
import javafx.scene.control.ContentDisplay
import javafx.scene.control.Label
import javafx.scene.input.KeyCode
import javafx.scene.paint.Color
import javafx.scene.shape.Rectangle
import javafx.scene.text.Font
import javafx.scene.text.FontWeight

const val BALL_SIZE = 10.0
const val BALL_SPEED = 1.5

const val PADDLE_WIDTH = 75.0
const val PADDLE_HEIGHT = 10.0
const val PADDLE_SPEED = 2.5

const val BRICK_WIDTH = 40.0
const val BRICK_HEIGHT = 20.0

enum class EntityTypes {
    PADDLE,
    BALL,
    BRICK
}

class BasicGame : GameApplication() {
    lateinit var paddle: Entity
    lateinit var ball: Entity
    private var numBricks = 0

    override fun initSettings(settings: GameSettings) {
        with(settings) {
            title = "Arkanoid Clone"
            version = "1.0.0"
            width = 800
            height = 600
            isMenuEnabled = true
            sceneFactory = MenuFactory()
        }
    }

    override fun initGame() {
        getGameWorld().addEntityFactory(ArkanoidEntityFactory())

        paddle = FXGL.entityBuilder()
            .type(EntityTypes.PADDLE)
            .at((getAppWidth() / 2) - PADDLE_WIDTH / 2, getAppHeight() - 50.0)
            .viewWithBBox(Rectangle(PADDLE_WIDTH, PADDLE_HEIGHT, Color.GRAY))
            .buildAndAttach()

        ball = spawn("ball", getAppWidth() / 2 - BALL_SIZE / 2, getAppHeight() / 2 - BALL_SIZE / 2)

        for (i in 0..9) {
            val x = ((getAppWidth() / 10) * i) + BRICK_WIDTH / 2
            for (y in 1..5) {
                spawn("brick", x, y * (BRICK_HEIGHT * 1.5) + 25)
                numBricks += 1
            }
        }

    }

    override fun initInput() {
        val input: Input = FXGL.getInput()

        input.onAction(KeyCode.D, "Move Right") {
            paddle.translateX(PADDLE_SPEED)
            if (paddle.rightX >= getAppWidth()) {
                paddle.x = getAppWidth() - PADDLE_WIDTH
            }
        }

        input.onAction(KeyCode.A, "Move Left") {
            paddle.translateX(-PADDLE_SPEED)
            if (paddle.x <= 0) {
                paddle.x = 0.0
            }
        }
    }

    override fun initUI() {
        val gameScene = FXGL.getGameScene()
        gameScene.setBackgroundColor(Color.LIGHTGRAY)

        val scoreText = getUIFactory().newText("", Color.BLACK, FontType.MONO, 20.0).apply {
            textProperty().bind(FXGL.getWorldProperties().intProperty("score").asString())
        }

        gameScene.addUINode(Label("SCORE:", scoreText).apply {
            font = Font.font("Monospaced", FontWeight.BOLD, 20.0)
            translateX = 10.0
            translateY = 10.0
            contentDisplay = ContentDisplay.RIGHT
        })
    }

    override fun initGameVars(vars: MutableMap<String, Any>) {
        vars["score"] = 0
    }

    override fun initPhysics() {
        onCollisionBegin(EntityTypes.BALL, EntityTypes.BRICK) { ball, brick ->
            brick.removeFromWorld()
            getGameState().increment("score", 10)
            numBricks -= 1

            if (numBricks == 0) {
                ball.setVelocity(0.0, 0.0)
                scoreNotification()
            } else {
                val velocity = ball.getObject<Point2D>("velocity")
                if (FXGLMath.randomBoolean()) {
                    ball.setVelocity(velocity.x, -velocity.y)
                } else {
                    ball.setVelocity(-velocity.x, velocity.y)
                }
            }
        }
    }

    override fun onUpdate(tpf: Double) {
        val velocity = ball.getObject<Point2D>("velocity")
        ball.translate(velocity)

        if (ball.boundingBoxComponent.isCollidingWith(paddle.boundingBoxComponent)) {
            ball.setVelocity(velocity.x, -velocity.y)
        }

        // Bounce from ze top
        if (ball.y <= BALL_SIZE / 2) {
            ball.y = BALL_SIZE / 2
            ball.setVelocity(velocity.x, -velocity.y)
        }

        // Bounce from left wall
        if (ball.x <= ball.width / 2) {
            ball.x = ball.width / 2
            ball.setVelocity(-velocity.x, velocity.y)
        }

        // Bounce from right wall
        if (ball.x >= getAppWidth()) {
            ball.x = getAppWidth() - (ball.width / 2)
            ball.setVelocity(-velocity.x, velocity.y)
        }

        // Hit the bottom D:
        if (ball.bottomY >= getAppHeight()) {
            ball.setVelocity(0.0, 0.0)
            scoreNotification()
        }
    }

    private fun scoreNotification() {
        // TODO: Something better than this
        getNotificationService().pushNotification(
            "Your final score: ${FXGL.getWorldProperties().intProperty("score").get()}"
        )
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            launch(BasicGame::class.java, args)
        }
    }
}

/**
 * Convenience method for setting velocity
 */
fun Entity.setVelocity(x: Double, y: Double) {
    setProperty("velocity", Point2D(x, y))
}

/**
 * Convenience method for just attaching onAction()
 */
private inline fun Input.onAction(keyCode: KeyCode, name: String, crossinline action: () -> Unit) {
    addAction(object : UserAction(name) {
        override fun onAction() {
            action()
        }
    }, keyCode)
}