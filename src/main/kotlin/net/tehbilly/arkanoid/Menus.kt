package net.tehbilly.arkanoid

import com.almasb.fxgl.app.scene.FXGLDefaultMenu
import com.almasb.fxgl.app.scene.FXGLMenu
import com.almasb.fxgl.app.scene.MenuType
import com.almasb.fxgl.app.scene.SceneFactory

class MenuFactory : SceneFactory() {
    override fun newMainMenu(): FXGLMenu {
        return FXGLDefaultMenu(MenuType.MAIN_MENU)
    }
}